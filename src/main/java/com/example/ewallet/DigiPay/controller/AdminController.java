package com.example.ewallet.DigiPay.controller;

import com.example.ewallet.DigiPay.entity.User;
import com.example.ewallet.DigiPay.entity.Wallet;
import com.example.ewallet.DigiPay.service.UserService;
import com.example.ewallet.DigiPay.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api/login/admin")
public class AdminController {

    private WalletService walletService;
    private UserService userService;

    @Autowired
    public AdminController(WalletService walletService, UserService userService) {
        this.walletService = walletService;
        this.userService = userService;
    }

    @GetMapping("/getAllWallets")
    public List<Wallet> getAllWallet(){
        return this.walletService.getAllWallet();
    }

    @GetMapping("/getAllUsers")
    public List<User> getUsers()
    {
        return this.userService.getUsers();
    }

}
