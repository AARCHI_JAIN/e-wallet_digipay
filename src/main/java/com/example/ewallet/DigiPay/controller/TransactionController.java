package com.example.ewallet.DigiPay.controller;


import com.example.ewallet.DigiPay.entity.Transaction;
import com.example.ewallet.DigiPay.entity.Wallet;
import com.example.ewallet.DigiPay.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class TransactionController {

    @Autowired
    TransactionService transactionService;


    @PostMapping("/sendMoney")
    public ResponseEntity<Transaction> sendMoney(@RequestBody Transaction transaction) {
        return new ResponseEntity<>(transactionService.sendMoney(transaction), HttpStatus.OK);
    }

    @PostMapping("/addMoneyFromAccount")
    public ResponseEntity<Transaction> addMoneyFromAccount(@RequestBody Transaction transaction) {
        return new ResponseEntity<>(transactionService.addMoneyFromAccount(transaction), HttpStatus.OK);
    }

    @PutMapping("/{id}/add/{amount}")
    public ResponseEntity<Wallet> addBalance(@PathVariable Integer id, @PathVariable Integer amount) {
        return ResponseEntity.ok(transactionService.addBalance(amount, id));
    }

    @GetMapping("/findBalance/{id}")
    public ResponseEntity<Double> getBalance(@PathVariable int id) {
        return ResponseEntity.ok(transactionService.getBalance(id));
    }

    @GetMapping("/txnHistory/{id}")
    public ResponseEntity<String> getHistory(@PathVariable long id){
        transactionService.sendTxnHistory(id);
        return ResponseEntity.ok("History sent....You will get the file on your email");
    }

}
