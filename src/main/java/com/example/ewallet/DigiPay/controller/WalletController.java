package com.example.ewallet.DigiPay.controller;
import com.example.ewallet.DigiPay.entity.Wallet;
import com.example.ewallet.DigiPay.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api/wallet")
public class WalletController {

    @Autowired
    private WalletService walletService;

    @GetMapping("/findOneWallet/{id}")
    public ResponseEntity<Wallet> getOneWallet(@PathVariable int id) {

        return ResponseEntity.ok(walletService.getOne(id));
    }

    @GetMapping("/findByUserId/{id}")
    public ResponseEntity<Wallet> getByUserId(@PathVariable int id) {

        return ResponseEntity.ok(walletService.findByUserId(id));
    }

    @DeleteMapping("/deleteWallet/{id}")
    public ResponseEntity<Wallet> deleteWallet(@PathVariable int id) {
        return ResponseEntity.ok(walletService.deleteWallet(id));
    }

    @PutMapping("/updateWallet/{id}")
    public ResponseEntity<Wallet> updateWallet(@RequestBody Wallet wallet, @PathVariable Integer id) {
        return ResponseEntity.ok(walletService.updateWallet(wallet,id));
    }


}
