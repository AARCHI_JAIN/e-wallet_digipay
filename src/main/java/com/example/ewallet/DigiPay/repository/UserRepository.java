package com.example.ewallet.DigiPay.repository;
import com.example.ewallet.DigiPay.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
}
