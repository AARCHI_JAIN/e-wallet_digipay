package com.example.ewallet.DigiPay.repository;

import com.example.ewallet.DigiPay.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface BankAccountRepository extends JpaRepository<Account, Integer> {

    @Query("Select u from Account u where u.accountNumber = ?1")
    Account findByAccountNumber(long id);

}

