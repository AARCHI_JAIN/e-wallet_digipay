package com.example.ewallet.DigiPay.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collection;


@Entity
@Data
@Table(name = "WalletAccount", schema = "e_wallet")
public class Account {


    @Id
    private int id;

    @Column(name = "accountNumber")
    private long accountNumber;

    @Column(name = "accountBalance")
    private double accountBalance;

    @ManyToMany(targetEntity = Wallet.class, cascade = CascadeType.ALL)
    @JsonIgnore
    private Collection<Wallet> wallet =new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public Collection<Wallet> getWallet() {
        return wallet;
    }

    public void setWallet(Collection<Wallet> wallet) {
        this.wallet = wallet;
    }
}
