package com.example.ewallet.DigiPay.service;
import com.example.ewallet.DigiPay.entity.Wallet;

import java.util.List;
import java.util.Optional;

public interface WalletService {

    List<Wallet> getAllWallet();
    Wallet getOne(int id);
    Wallet findByUserId(int id);
    Wallet deleteWallet(int id);
    Wallet updateWallet(Wallet wallet, int id);

}
