package com.example.ewallet.DigiPay.service.impl;

import com.example.ewallet.DigiPay.entity.Account;
import com.example.ewallet.DigiPay.entity.Transaction;
import com.example.ewallet.DigiPay.entity.User;
import com.example.ewallet.DigiPay.entity.Wallet;
import com.example.ewallet.DigiPay.exception.model.AccountNotFoundException;
import com.example.ewallet.DigiPay.exception.model.InsufficientBalanceInAccountException;
import com.example.ewallet.DigiPay.exception.model.InsufficientBalanceInWalletException;
import com.example.ewallet.DigiPay.exception.model.TransactionBadRequestException;
import com.example.ewallet.DigiPay.repository.BankAccountRepository;
import com.example.ewallet.DigiPay.repository.TransactionRepository;
import com.example.ewallet.DigiPay.service.SmsService;
import com.example.ewallet.DigiPay.service.TransactionService;
import com.example.ewallet.DigiPay.service.UserService;
import com.example.ewallet.DigiPay.service.WalletService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileWriter;

import java.time.LocalDate;
import java.util.ArrayList;

@Service
@Slf4j
public class TransactionServiceImpl implements TransactionService {

    private TransactionRepository transactionRepository;
    private BankAccountRepository bankAccountRepository;
    private UserService userService;
    private WalletService walletService;
    private SmsService smsService;


    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository, BankAccountRepository bankAccountRepository, UserService userService, WalletService walletService, SmsService smsService) {
        this.transactionRepository = transactionRepository;
        this.bankAccountRepository = bankAccountRepository;
        this.userService = userService;
        this.walletService = walletService;
        this.smsService = smsService;
    }

    @Override
    @Transactional
    public Transaction sendMoney (Transaction transaction){
        LocalDate date = LocalDate.now();
        transaction.setDate(date);

        double amt = transaction.getAmount();
        if (amt < 0) {
            throw new TransactionBadRequestException(+amt+" (Not a Valid AMOUNT TO TRANSFER)");
        }

        User sender = userService.getOne(Math.toIntExact(transaction.getSid()));
        User receiver = userService.getOne(Math.toIntExact(transaction.getRid()));

        Wallet senderWallet = walletService.findByUserId(sender.getId());
        Wallet receiverWallet = walletService.findByUserId(receiver.getId());

        if (senderWallet.getBalance() < amt) {
            throw new InsufficientBalanceInWalletException(senderWallet.getId()," - Wallet does not have sufficient balance");
        }

        senderWallet.setBalance(senderWallet.getBalance()-amt);
        receiverWallet.setBalance(receiverWallet.getBalance()+amt);
        transaction.setStatus("SUCCESS");
        walletService.updateWallet(senderWallet, senderWallet.getId());
        walletService.updateWallet(receiverWallet, receiverWallet.getId());

        if (senderWallet.getBalance() <= 0 ) {
            throw new InsufficientBalanceInWalletException(senderWallet.getId()," - Wallet does not have sufficient balance");
        }
        EmailServiceImpl.sendEmailToUser(sender.getEmail(), "Payment Successful to "+ receiver.getMobile()+" of Rs." + amt + " Total Balance left is: " + senderWallet.getBalance());
        smsService.sendSms(sender.getMobile(), "Payment Successful to "+ receiver.getMobile()+" of Rs." + amt + " Total Balance left is: " + senderWallet.getBalance());
        smsService.sendSms(receiver.getMobile(), "Payment Received from "+ sender.getMobile()+" of Rs." + amt + " Total Balance is: "+ receiverWallet.getBalance());
        EmailServiceImpl.sendEmailToUser(receiver.getEmail(), "Payment Received from "+ sender.getMobile()+" of Rs." + amt + " Total Balance is: "+ receiverWallet.getBalance());


        log.info("Wallet-To-Wallet Transaction Successful");
        return transactionRepository.save(transaction);

    }

    @Override
    public Wallet addBalance(int amount, int id){
        Wallet wallet = walletService.getOne(id);
        wallet.setBalance(wallet.getBalance()+amount);
        return walletService.updateWallet(wallet,id);

    }

    @Override
    public Double getBalance(int id) {
        Wallet wallet = walletService.getOne(id);
        return wallet.getBalance();
    }

    @Override
    @Transactional
    public Transaction addMoneyFromAccount (Transaction transaction){
        LocalDate date = LocalDate.now();
        transaction.setDate(date);

        double amt = transaction.getAmount();
        if (amt < 0) {
            throw new TransactionBadRequestException(+amt+" (Not a Valid AMOUNT TO TRANSFER)");
        }

        Wallet addMoneyWallet = walletService.getOne(Math.toIntExact(transaction.getRid()));
        Account account = bankAccountRepository.findByAccountNumber(transaction.getSid());
        if(account==null){
            throw new AccountNotFoundException(transaction.getSid()," - NO ACCOUNT FOUND WITH GIVEN ACCOUNT NUMBER ");
        }

        double amount = transaction.getAmount();
        if (account.getAccountBalance() < amount) {
            throw new InsufficientBalanceInAccountException(Math.toIntExact(account.getAccountNumber())," - Account does not have sufficient balance");
        }

        account.setAccountBalance(account.getAccountBalance()-amount);
        addMoneyWallet.setBalance(addMoneyWallet.getBalance()+amount);
        transaction.setStatus("SUCCESS");

        walletService.updateWallet(addMoneyWallet,addMoneyWallet.getId());
        bankAccountRepository.save(account);
        if (account.getAccountBalance() <= 0 ) {
            throw new InsufficientBalanceInAccountException(Math.toIntExact(account.getAccountNumber())," - Account does not have sufficient balance");
        }
        smsService.sendSms(userService.getOne(Math.toIntExact(transaction.getRid())).getMobile(), "Payment Received from Account Number: "+ account.getAccountNumber()+ " of Rs." + amount + " Total Balance in DigiPay wallet is: "+ addMoneyWallet.getBalance());
        EmailServiceImpl.sendEmailToUser(userService.getOne(Math.toIntExact(transaction.getRid())).getEmail(), "Payment Transferred to "+ addMoneyWallet.getUser().getFirstName()+" of Rs." + amount + " Total Balance left is: " + account.getAccountBalance());


        log.info("Account-To-Wallet Transaction Successful - Balance added successfully in Wallet");
        return transactionRepository.save(transaction);
    }


    @Override
    public String sendTxnHistory(long id) {
        ArrayList<Transaction> list = (ArrayList<Transaction>) transactionRepository.findBySidAndRid(id);
        User user = userService.getOne(Math.toIntExact(id));
        String filename ="DigiPay.csv";
        FileWriter fw = null;
        try {
            fw = new FileWriter(filename);

            for(int i=0;i<list.size();i++) {
                fw.append(list.get(i).getStatus());
                fw.append(',');
                double amt = list.get(i).getAmount();
                Double obj = amt;
                fw.append(obj.toString());
                fw.append(',');
                fw.append(list.get(i).getDate().toString());
                fw.append(',');
                int id2 = list.get(i).getId();
                Integer obj2 = id2;
                fw.append(obj2.toString());
                fw.append(',');
                long rid = list.get(i).getRid();
                obj = Double.valueOf(rid);
                fw.append(obj.toString());
                fw.append(',');
                long sid = list.get(i).getSid();
                obj = Double.valueOf(sid);
                fw.append(obj.toString());
                fw.append('\n');
            }
            fw.close();
            EmailServiceImpl.sendEmailWithAttachments("","",user.getEmail(),"","",filename);
            smsService.sendSms(user.getMobile(), "Please find your transaction history of DigiPay on your registered Email address!!");
        } catch (Exception e) {
            log.error("History Not Found");
        }
        log.info("Transaction History Sent Successfully");
        return "Please find your transaction history of DigiPay on your registered Email address!!";
    }
}
