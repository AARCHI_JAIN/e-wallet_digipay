package com.example.ewallet.DigiPay.service.impl;

import com.example.ewallet.DigiPay.configuration.EmailConfiguration;
import org.springframework.stereotype.Service;

import javax.mail.Authenticator;
import javax.mail.Message;

import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

@Service
public class EmailServiceImpl {

    public static  final String FROM_EMAIL = "deckhushboojain@gmail.com";
    public static final String PASSWORD = "DigiPay@123";

    public static void sendEmailToUser(String toEmail, String body)
    {
        Properties props = getProperties();
        Authenticator auth = getAuthenticator();
        Session session = Session.getDefaultInstance(props, auth);
        EmailConfiguration.sendEmail(session, toEmail,"Mail from DigiPay", body);
    }

    public static void sendEmailWithAttachments(String host, String port, String toAddress,
                                                String subject, String message, String attachFiles)
    {
        subject = getDefaultSubject();
        message = getDefaultMessage();

        Properties properties = getProperties();
        // creates a new session with an authenticator
        try
        {
            Authenticator auth = getAuthenticator();
            Session session = Session.getInstance(properties, auth);

            // creates a new e-mail message
            Message msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress("deckhushboojain@gmail.com", "NoReply- Message From DigiPay"));

            msg.setReplyTo(InternetAddress.parse("deckhushboojain@gmail.com", false));
            
            InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
            msg.setRecipients(Message.RecipientType.TO, toAddresses);
            msg.setSubject(subject);
            msg.setSentDate(new Date());

            // creates message part
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(message, "text/html");

            // creates multi-part
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

            // adds attachments
            String filePath = attachFiles;

            MimeBodyPart attachPart = new MimeBodyPart();

            try {
                attachPart.attachFile(filePath);
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            multipart.addBodyPart(attachPart);

            // sets the multi-part as e-mail's content
            msg.setContent(multipart);
            // sends the e-mail
            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", FROM_EMAIL, PASSWORD);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
        }
        catch (Exception e)
        {
            e.getMessage();
        }

    }

    private static Properties getProperties()
    {
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        return properties;
    }
    private static Authenticator getAuthenticator()
    {
        Authenticator auth = new Authenticator() {
            //override the getPasswordAuthentication method
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(FROM_EMAIL, PASSWORD);
            }
        };
        return auth;
    }

    private static String getDefaultSubject()
    {
        String subject1 = "Mail from e_wallet(DigiPay)";
        return subject1;
    }

    private static String getDefaultMessage()
    {
        String message1 = "Please find the attached file of your transaction history!!";
        return message1;
    }
}
