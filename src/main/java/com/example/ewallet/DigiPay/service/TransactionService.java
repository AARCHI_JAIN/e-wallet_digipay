package com.example.ewallet.DigiPay.service;

import com.example.ewallet.DigiPay.entity.Transaction;
import com.example.ewallet.DigiPay.entity.Wallet;

public interface TransactionService {

    Transaction sendMoney (Transaction transaction);
    Wallet addBalance(int amount, int id);
    Double getBalance(int id);
    Transaction addMoneyFromAccount (Transaction transaction);
    String sendTxnHistory(long id);
}
