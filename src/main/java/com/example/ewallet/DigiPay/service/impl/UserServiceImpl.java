package com.example.ewallet.DigiPay.service.impl;
import com.example.ewallet.DigiPay.entity.User;
import com.example.ewallet.DigiPay.exception.model.UserNotFoundException;
import com.example.ewallet.DigiPay.repository.UserRepository;
import com.example.ewallet.DigiPay.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getOne(int id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id , " - USER NOT FOUND IN DATABASE"));
    }

    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User deleteUser(int id) {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id, " (User Not Found) "));
        userRepository.deleteById(id);
        return user;
    }

    @Override
    public User updateUser(User user, int id){
        userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id, " (User Not Found) "));
        user.setId(id);
        return userRepository.save(user);
    }
}
