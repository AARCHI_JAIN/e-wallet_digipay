package com.example.ewallet.DigiPay.service.impl;
import com.example.ewallet.DigiPay.service.SmsService;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.stereotype.Service;

@Service
public class SmsServiceImpl implements SmsService {

    @Override
    public void sendSms(String phoneNumber, String body) {

        Message message = Message.creator(new PhoneNumber(phoneNumber),
                new PhoneNumber("+12155150376"),
                body).create();


    }
}
