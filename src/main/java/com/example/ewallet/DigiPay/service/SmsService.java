package com.example.ewallet.DigiPay.service;

public interface SmsService {

    void sendSms(String phoneNumber, String body);
}
