package com.example.ewallet.DigiPay.service.impl;
import com.example.ewallet.DigiPay.entity.User;
import com.example.ewallet.DigiPay.entity.Wallet;
import com.example.ewallet.DigiPay.exception.model.UserNotFoundException;
import com.example.ewallet.DigiPay.exception.model.WalletNotFoundException;
import com.example.ewallet.DigiPay.repository.WalletRepository;
import com.example.ewallet.DigiPay.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WalletServiceImpl implements WalletService {

    @Autowired
    private WalletRepository walletRepository;


    @Override
    public List<Wallet> getAllWallet() {
        return walletRepository.findAll();
    }

    @Override
    public Wallet getOne(int id) {
        return walletRepository.findById(id)
                .orElseThrow(() -> new WalletNotFoundException(id, " Wallet Not Found"));
    }

    @Override
    public Wallet findByUserId(int id) {
        return walletRepository.findByUserId(id);
    }


    @Override
    public Wallet deleteWallet(int id) {
        Wallet wallet = walletRepository.findById(id).orElseThrow(() -> new WalletNotFoundException(id, " (Wallet Not Found) "));
        walletRepository.deleteById(id);
        return wallet;
    }

    @Override
    public Wallet updateWallet(Wallet wallet, int id) {
        walletRepository.findById(id)
        .orElseThrow( () ->new WalletNotFoundException(id, " Wallet Not Found"));
        wallet.setId(id);
        return walletRepository.save(wallet);
    }
}
