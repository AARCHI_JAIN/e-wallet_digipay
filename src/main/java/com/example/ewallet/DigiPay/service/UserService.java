package com.example.ewallet.DigiPay.service;
import com.example.ewallet.DigiPay.entity.User;


import java.util.List;

public interface UserService {

    List<User> getUsers();
    User getOne(int id);
    User createUser(User user);
    User deleteUser(int id);
    User updateUser(User user, int id);
}
