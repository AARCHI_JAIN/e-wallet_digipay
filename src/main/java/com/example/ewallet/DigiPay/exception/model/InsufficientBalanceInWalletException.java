package com.example.ewallet.DigiPay.exception.model;

public class InsufficientBalanceInWalletException extends RuntimeException {

    public InsufficientBalanceInWalletException(int id, String message){
        super(id + message);
    }
}
