package com.example.ewallet.DigiPay.exception.model;

public class UserNotFoundException extends RuntimeException{

    public UserNotFoundException(String message){
        super(message);
    }

    public UserNotFoundException(int id , String message){
        super(id + message);
    }
}
