package com.example.ewallet.DigiPay.exception.model;

public class TransactionBadRequestException extends RuntimeException{

    public TransactionBadRequestException(String message){
        super(message);
    }
}
