package com.example.ewallet.DigiPay.exception.model;

public class InsufficientBalanceInAccountException extends RuntimeException{

    public InsufficientBalanceInAccountException(int id, String message){
        super(id + message);
    }
}
