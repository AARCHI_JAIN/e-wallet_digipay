package com.example.ewallet.DigiPay.exception.handler;

import com.example.ewallet.DigiPay.exception.model.AccountNotFoundException;
import com.example.ewallet.DigiPay.exception.model.Error;
import com.example.ewallet.DigiPay.exception.model.InsufficientBalanceInAccountException;
import com.example.ewallet.DigiPay.exception.model.InsufficientBalanceInWalletException;
import com.example.ewallet.DigiPay.exception.model.TransactionBadRequestException;
import com.example.ewallet.DigiPay.exception.model.UserNotFoundException;
import com.example.ewallet.DigiPay.exception.model.WalletNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import javax.servlet.http.HttpServletRequest;


@ControllerAdvice
@Slf4j
public class EWalletExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UserNotFoundException.class)
    protected ResponseEntity<Error> handleException(UserNotFoundException ex, HttpServletRequest request) {
        log.error("User Not Found Exception");
        Error error = Error.builder()
                .code("User Not Found")
                .message("For id given:- " +ex.getMessage())
                .build();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }

    @ExceptionHandler(WalletNotFoundException.class)
    protected ResponseEntity<Error> handleException(WalletNotFoundException ex, HttpServletRequest request) {
       log.error("Wallet Not Found Exception");
        Error error = Error.builder()
                .code("Wallet Not Found")
                .message("For id given:- " +ex.getMessage())
                .build();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }

    @ExceptionHandler(InsufficientBalanceInWalletException.class)
    protected ResponseEntity<Error> handleException(InsufficientBalanceInWalletException ex, HttpServletRequest request) {
        log.error("Insufficient Balance In Wallet Exception");
        Error error = Error.builder()
                .code("Insufficient Balance In Wallet")
                .message("For id given:- " +ex.getMessage())
                .build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    @ExceptionHandler(AccountNotFoundException.class)
    protected ResponseEntity<Error> handleException(AccountNotFoundException ex, HttpServletRequest request) {
        log.error("Account Not Found Exception");
        Error error = Error.builder()
                .code("Account Not Found")
                .message("For Account Number given:- " +ex.getMessage())
                .build();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }

    @ExceptionHandler(InsufficientBalanceInAccountException.class)
    protected ResponseEntity<Error> handleException(InsufficientBalanceInAccountException ex, HttpServletRequest request) {
        log.error("Insufficient Balance In Account Exception");
        Error error = Error.builder()
                .code("Insufficient Balance In Account")
                .message("For Account Number given:- " +ex.getMessage())
                .build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    @ExceptionHandler(TransactionBadRequestException.class)
    protected ResponseEntity<Error> handleException(TransactionBadRequestException ex, HttpServletRequest request) {
        log.error("Transaction Bad Request Exception");
        Error error = Error.builder()
                .code("Amount is Not Valid")
                .message("For Amount given: " +ex.getMessage())
                .build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }
}
