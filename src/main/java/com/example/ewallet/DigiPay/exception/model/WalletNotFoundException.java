package com.example.ewallet.DigiPay.exception.model;

public class WalletNotFoundException extends RuntimeException{

    public WalletNotFoundException(int id , String message){
        super(id + message);
    }
}
