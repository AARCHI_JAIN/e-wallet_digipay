package com.example.ewallet.DigiPay.exception.model;

public class AccountNotFoundException extends RuntimeException{

    public AccountNotFoundException(long id, String message){
        super(id + message);
    }
}
