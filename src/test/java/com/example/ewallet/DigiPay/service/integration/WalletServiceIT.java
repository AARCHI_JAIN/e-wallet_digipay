package com.example.ewallet.DigiPay.service.integration;


import com.example.ewallet.DigiPay.entity.User;
import com.example.ewallet.DigiPay.entity.Wallet;
import com.example.ewallet.DigiPay.service.UserService;
import com.example.ewallet.DigiPay.service.WalletService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
@RunWith(SpringRunner.class)
public class WalletServiceIT {

    @Autowired
    WalletService walletService;

    @Autowired
    UserService userService;

    @Test
    public  void getOneWalletTest() {
        Wallet wallet = walletService.getOne(2);
        assertNotNull(wallet);
        assertEquals(2,wallet.getId());
    }

    @Test
    public void getAllWalletTest() {
        List<Wallet> wallets = walletService.getAllWallet();
        assertNotNull(wallets);
    }

    @Test
    public  void getOneWalletByUserIdTest() {
        Wallet wallet = walletService.findByUserId(2);
        assertNotNull(wallet);
        assertEquals(2,wallet.getId());
    }


    @Test
    public void createUserTest(){
        User user = new User();
        user.setId(100);
        user.setFirstName("Khushi");
        user.setLastName("Garg");
        user.setMobile("8745848346");
        user.setEmail("deckhushoojain@gmail.com");
        Wallet wallet = new Wallet();
        wallet.setId(100);
        wallet.setBalance(10);
        wallet.setIs_active("true");
        user.setWallet(wallet);
        User user1 = userService.createUser(user);
        assertNotNull(user1);
    }

    @Test
    public void updateWalletTest(){
        Wallet wallet = new Wallet();
        wallet.setId(100);
        wallet.setBalance(900);
        wallet.setIs_active("true");
        Wallet wallet1 = walletService.updateWallet(wallet,100);
    }

}
