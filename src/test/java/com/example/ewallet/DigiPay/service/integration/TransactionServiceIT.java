package com.example.ewallet.DigiPay.service.integration;

import com.example.ewallet.DigiPay.entity.Transaction;
import com.example.ewallet.DigiPay.entity.Wallet;
import com.example.ewallet.DigiPay.service.TransactionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@SpringBootTest
@RunWith(SpringRunner.class)
public class TransactionServiceIT {

    @Autowired
    private TransactionService transactionService;

    @Test
    public  void sendMoneyTest() {
        Transaction transaction = new Transaction();
        transaction.setAmount(10);
        transaction.setSid(2);
        transaction.setRid(3);
        transaction.setStatus("SUCCESS");
        Transaction transaction1 = transactionService.sendMoney(transaction);
        assertNotNull(transaction1);
        assertEquals(2,transaction1.getSid());
    }


    @Test
    public  void addMoneyFromAccountTest() {
        Transaction transaction = new Transaction();
        transaction.setAmount(100);
        transaction.setSid(1246545709);
        transaction.setRid(2);
        transaction.setStatus("SUCCESS");
        Transaction transaction1 = transactionService.addMoneyFromAccount(transaction);
        assertNotNull(transaction1);
        assertEquals(2,transaction1.getRid());
    }

    @Test
    public  void addBalanceTest() {
        Wallet transaction1 = transactionService.addBalance(100,2);
        assertNotNull(transaction1);
        assertEquals(2,transaction1.getId());
    }

    @Test
    public  void getBalanceTest() {
        Double transaction1 = transactionService.getBalance(1);
        assertNotNull(transaction1);
    }

    @Test
    public  void transactionHistoryTest() {
        String txnHistory = transactionService.sendTxnHistory(2);
        assertNotNull(txnHistory);
    }
}
