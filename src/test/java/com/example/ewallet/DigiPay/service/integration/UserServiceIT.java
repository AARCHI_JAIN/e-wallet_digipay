package com.example.ewallet.DigiPay.service.integration;


import com.example.ewallet.DigiPay.entity.User;
import com.example.ewallet.DigiPay.entity.Wallet;
import com.example.ewallet.DigiPay.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceIT {

    @Autowired
    private UserService userService;

    @Test
    public  void getOneUserTest() {
        User user1 = userService.getOne(1);
        assertNotNull(user1);
        assertEquals(1,user1.getId());
    }

    @Test
    public void getAllUsersTest() {
        List<User> users = userService.getUsers();
        assertNotNull(users);
    }

    @Test
    public void createUserTest(){
        User user = new User();
        user.setId(10);
        user.setFirstName("Khushi");
        user.setLastName("Garg");
        user.setMobile("8745848346");
        user.setEmail("deckhushoojain@gmail.com");
        Wallet wallet = new Wallet();
        wallet.setId(10);
        wallet.setBalance(1000);
        wallet.setIs_active("true");
        user.setWallet(wallet);
        User user1 = userService.createUser(user);
        assertNotNull(user1);
    }

    @Test
    public void updateUserTest(){
        User user = new User();
        user.setId(10);
        user.setFirstName("Khushi");
        user.setLastName("Jain");
        user.setMobile("+918745848346");
        user.setEmail("deckhushoo@gmail.com");
        Wallet wallet = new Wallet();
        wallet.setId(10);
        wallet.setBalance(1000);
        wallet.setIs_active("true");
        user.setWallet(wallet);
        User user1 = userService.createUser(user);
        assertNotNull(user1);
        assertEquals(10,user1.getId());
    }

    @Test
    public void deleteUserTest(){
        User user = userService.deleteUser(10);
        assertEquals(10, user.getId());
    }
}
